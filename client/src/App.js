import React, { Component } from 'react';
// components
import BookList from './components/BookList';
import AddBook from './components/AddBook';
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'



class App extends Component {
  render() {
    return (
      
            <div id="main">
              <ReactNotification />
                <h1>Yosser's Reading List</h1>
                <BookList />
                <AddBook />
            </div>
    );
  }
}

export default App;
